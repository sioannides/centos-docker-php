centos-docker-php
========================

  

Developed by [Stelios Ioannides](https://stelios.ioannides.info).

### Documentation

Use relevant branch for relevant PHP version

### License

[MIT](https://bitbucket.org/sioannides/centos-docker/src/master/LICENSE)

### Links

* Bitbucket: https://bitbucket.org/sioannides/centos-docker-php
